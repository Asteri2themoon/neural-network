#include "neuralNetwork.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <ctime>

neuralNetwork::neuralNetwork()
{
    n_size=0;
    n_correction=0;
    n_connected=false;
    n_layer=NULL;
	n_neuronStateLayer=NULL;
    n_neuronState=NULL;
    n_matrix=NULL;
	n_matrixLayer=NULL;
}

neuralNetwork::neuralNetwork(unsigned int s,unsigned int *nl)
{
    unsigned int neuronSize=0,i;
    n_size=s;
    n_correction=0;
    n_connected=false;
    n_layer=new unsigned int[s];
	n_neuronStateLayer=new unsigned int[s];
    for(i=0;i<s;i++)
    {
		n_neuronStateLayer[i]=neuronSize;
        n_layer[i]=nl[i];
        neuronSize+=nl[i];
    }
    n_neuronState=new float[neuronSize];
    for(i=0;i<neuronSize;i++)
        n_neuronState[i]=0;
    n_matrix=NULL;
	n_matrixLayer=NULL;
}

void neuralNetwork::connect()
{
	unsigned int matrixSize=0,i;
    if(n_matrix!=NULL)
        delete n_matrix;
    if(n_matrixLayer!=NULL)
        delete n_matrixLayer;
	n_matrixLayer = new unsigned int[n_size];
	n_matrixLayer[0]=0;
	for(i=1;i<n_size;i++)
	{
		matrixSize+=n_layer[i-1]*n_layer[i];
		n_matrixLayer[i]=matrixSize;
	}
	n_matrix = new float[matrixSize];
	for(i=0;i<matrixSize;i++)
		n_matrix[i]=(float)((rand()%200)-100)/1000;
	n_connected=true;
}

void neuralNetwork::setSize(unsigned int size)
{
    unsigned int i;
	n_connected=false;
    if(n_layer!=NULL)
        delete n_layer;
    if(n_neuronState!=NULL)
        delete n_neuronState;
    n_size=size;
    n_layer=new unsigned int[size];
    n_neuronState=new float[size];
    for(i=0;i<size;i++)
    {
        n_layer[i]=1;
        n_neuronState[i]=0;
    }
}

void neuralNetwork::setSize(unsigned int s,unsigned int *nl)
{
    unsigned int neuronSize=0,i;
	n_connected=false;
    if(n_layer!=NULL)
        delete n_layer;
    if(n_neuronStateLayer!=NULL)
        delete n_neuronState;
    if(n_neuronState!=NULL)
        delete n_neuronState;
    n_size=s;
    n_layer=new unsigned int[s];
	n_neuronStateLayer=new unsigned int[s];
    for(i=0;i<s;i++)
    {
		n_neuronStateLayer[i]=neuronSize;
        n_layer[i]=nl[i];
        neuronSize+=nl[i];
    }
    n_neuronState=new float[neuronSize];
    for(i=0;i<neuronSize;i++)
        n_neuronState[i]=0;
}

unsigned int neuralNetwork::getSize(){
	return n_size;
}

void neuralNetwork::setLayer(unsigned int l,unsigned int nS)
{
    unsigned int neuronSize=0,i;
	n_connected=false;
    if(n_neuronStateLayer!=NULL)
        delete n_neuronStateLayer;
    if(n_neuronState!=NULL)
        delete n_neuronState;
    n_layer[l]=nS;
	n_neuronStateLayer=new unsigned int[n_size];
    for(i=0;i<n_size;i++)
	{
		n_neuronStateLayer[i]=neuronSize;
        neuronSize+=n_layer[i];
	}
    n_neuronState=new float[neuronSize];
    for(i=0;i<neuronSize;i++)
        n_neuronState[i]=0;
}

unsigned int neuralNetwork::getLayerSize(unsigned int l){
	return n_layer[l];
}

void neuralNetwork::setNeuron(unsigned int l,unsigned int n,float value){
	n_neuronState[n_neuronStateLayer[l]+n]=value;
}

float neuralNetwork::getNeuron(unsigned int l,unsigned int n){
	return n_neuronState[n_neuronStateLayer[l]+n];
}

void neuralNetwork::setConnection(unsigned int l,unsigned int n1,unsigned int n2,float ratio){
	n_matrix[n_matrixLayer[l]+(n_layer[l+1]*n1)+n2]=ratio;
}

float neuralNetwork::getConnection(unsigned int l,unsigned int n1,unsigned int n2){
	return n_matrix[n_matrixLayer[l]+(n_layer[l+1]*n1)+n2];
}
void neuralNetwork::setCorrection(float c){
    n_correction=c;
}
float neuralNetwork::getCorrection(){
    return n_correction;
}

bool neuralNetwork::save(char* dir)
{
	if(!n_connected)
		return false;
	std::ofstream file;
	int size,fileSize,c,n,s,ratio,i,matSize=n_matrixLayer[n_size-1];
	char *buffer = NULL,*pos,*weight;
	file.open(dir,std::ios::binary);
	if(!file)
		return false;
	size=getSize();
	fileSize=4+size*4;
	for(c=0;c<size-1;c++)
		fileSize+=(getLayerSize(c)*getLayerSize(c+1))*4;
	buffer = new char[fileSize];
	buffer[0]=size,buffer[1]=size>>8,buffer[2]=size>>16,buffer[3]=size>>24;
	for(c=0;c<size;c++)
		buffer[c*4+4]=getLayerSize(c),buffer[c*4+5]=getLayerSize(c)>>8,buffer[c*4+6]=getLayerSize(c)>>16,buffer[c*4+7]=getLayerSize(c)>>24;
    weight=(char*)n_matrix;
    pos=(char*)&buffer[size*4+4];
    for(i=0;i<(matSize*4);i++){
        (*(pos++))=*(weight++);
    }
	/*i=size*4+4;
	size--;
	for(c=0;c<size;c++)
		for(n=0;n<getLayerSize(c);n++)
			for(s=0;s<getLayerSize(c+1);s++)
			{
				ratio=getConnection(c,n,s)*10000;
				buffer[i]=ratio,buffer[i+1]=ratio>>8,buffer[i+2]=ratio>>16,buffer[i+3]=ratio>>24;
				i+=4;
			}*/
	file.write(buffer,fileSize);
	file.close();
	delete buffer;
	return true;
}

bool neuralNetwork::load(char* dir)
{
	std::ifstream file;
	float coef;
	int i,tailleRes,taille,y,c,n,s,matSize;
	unsigned int *configRes = NULL;
	char *buffer = NULL,*weight,*pos;
	file.open(dir,std::ios::binary);
	if(!file)
		return false;
	file.seekg(0,std::ios::end);
	taille=file.tellg();
	file.seekg(0,std::ios::beg);
	buffer = new char[taille];
	file.read(buffer,taille);
	file.close();
	tailleRes=(buffer[0]&0xFF)|((buffer[1]<<8)&0xFF00)|((buffer[2]<<16)&0xFF0000)|((buffer[3]<<24)&0xFF000000);
	configRes = new unsigned int[tailleRes];
	for(y=0;y<tailleRes;y++)
		configRes[y]=(buffer[4+y*4]&0xFF)|((buffer[5+y*4]<<8)&0xFF00)|((buffer[6+y*4]<<16)&0xFF0000)|((buffer[7+y*4]<<24)&0xFF000000);
	setSize(tailleRes,configRes);
	connect();

    matSize=n_matrixLayer[n_size-1];
	weight=(char*)n_matrix;
    pos=(char*)&buffer[n_size*4+4];
    for(i=0;i<(matSize*4);i++){
        (*(pos++))=*(weight++);
    }
	/*i=tailleRes*4+4;
	tailleRes--;
	for(c=0;c<tailleRes;c++)
		for(n=0;n<getLayerSize(c);n++)
			for(s=0;s<getLayerSize(c+1);s++)
			{
				coef=(buffer[i]&0xFF)|((buffer[i+1]<<8)&0xFF00)|((buffer[i+2]<<16)&0xFF0000)|((buffer[i+3]<<24)&0xFF000000);
				coef/=10000;
				setConnection(c,n,s,coef);
				i+=4;
			}*/
	delete configRes;
	delete buffer;
	return true;
}

void neuralNetwork::reset()
{
	unsigned int neuronSize=0,i;
    for(i=0;i<n_size;i++)
        neuronSize+=n_layer[i];
	for(i=0;i<neuronSize;i++)
		n_neuronState[i]=0;
}

void neuralNetwork::spreading()
{
	unsigned int i,n1,n2;
	float active;
	for(i=0;i<n_size-1;i++)
	{
		for(n2=0;n2<getLayerSize(i+1);n2++)
		{
			active=0;
			for(n1=0;n1<getLayerSize(i);n1++)
				active+=getConnection(i,n1,n2)*getNeuron(i,n1);
            if(i==(n_size-2)){
                setNeuron(i+1,n2,active);
            }
            else{
                setNeuron(i+1,n2,activate(active));
            }
		}
	}
}

void neuralNetwork::learn(float y)
{
	unsigned int neuronSize=0,i,c,n,s;
	float *delta=NULL;
	float sommeDelta;
    for(i=0;i<n_size;i++)
        neuronSize+=n_layer[i];
	delta = new float[neuronSize];
    //rétropropagation:
    for(c=n_size-1;c>0;c--)//calcul des deltas
    {
        if(c==(n_size-1))
            for(n=0;n<n_layer[c];n++)
            {
				delta[n_neuronStateLayer[c]+n]=y-n_neuronState[n_neuronStateLayer[c]+n];
            }
        else
            for(n=0;n<n_layer[c];n++)
            {
                sommeDelta=0;
                for(s=0;s<n_layer[c+1];s++)
                    sommeDelta+=delta[n_neuronStateLayer[c+1]+s]*getConnection(c,n,s);
                //delta[n_neuronStateLayer[c]+n]=n_neuronState[n_neuronStateLayer[c]+n]*(1-n_neuronState[n_neuronStateLayer[c]+n])*sommeDelta;
                delta[n_neuronStateLayer[c]+n]=deriv_activate(n_neuronState[n_neuronStateLayer[c]+n])*sommeDelta;
            }
    }
    for(c=0;c<(n_size-1);c++)//correction des poids des synapses
    {
        for(n=0;n<n_layer[c];n++)
            for(s=0;s<n_layer[c+1];s++)
            {
                setConnection(c,n,s,getConnection(c,n,s)+(n_correction*n_neuronState[n_neuronStateLayer[c]+n]*delta[n_neuronStateLayer[c+1]+s]));
            }
    }
	delete delta;
}

neuralNetwork::~neuralNetwork()
{
    if(n_layer!=NULL)
        delete n_layer;
    if(n_neuronStateLayer!=NULL)
        delete n_neuronStateLayer;
    if(n_neuronState!=NULL)
        delete n_neuronState;
    if(n_matrixLayer!=NULL)
        delete n_matrixLayer;
    if(n_matrix!=NULL)
        delete n_matrix;
}
