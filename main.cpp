#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <iomanip>
#include <windows.h>

using namespace std;

#include "neuralNetwork.h"

#define MIN -1
#define MAX 1
#define ITERATION 100
#define COLUMN_WIDTH 15
#define f(x) (x*x)
#define abs(x) ((x)>=0?(x):-(x))

void gotoXY(int x, int y);

float test(neuralNetwork *nn){
    float input,total=0;
    for(input=MIN;input<=MAX;input+=0.1){
        nn->setNeuron(0,0,input);
        nn->spreading();
        total+=abs(nn->getNeuron(nn->getSize()-1,0)-f(input));
    }
    return total;
}

int main()
{
    clock_t t0,t;
    float input,tRestant;
    int per1000=-1;
    int mod=100;
    unsigned int i,layer[]={1,50,10,1};
    //srand(time(NULL));
    neuralNetwork nn(sizeof(layer)/sizeof(unsigned int),layer);
    nn.connect();
    nn.setCorrection(0.01);

    t0=clock();
    while(!(GetKeyState(VK_ESCAPE) & 32768)){
        input=(float)((rand()%((MAX-MIN)*100))+(MIN*100))/100;
        nn.setNeuron(0,0,input);
        nn.spreading();
        nn.learn(f(input));
        if((clock()-t0)>40){
            gotoXY(0,0);
            cout << "erreur:" << setw(10) << test(&nn) << endl;
            t0=clock();
        }
    }
    system("cls");
    cout << "final test:" << endl;
    cout << setw(COLUMN_WIDTH) << "x" << setw(COLUMN_WIDTH) << "f(x)" << setw(COLUMN_WIDTH) << "network reply" << setw(COLUMN_WIDTH) << "error" <<endl;
    for(input=MIN;input<=MAX;input+=0.1){
        nn.setNeuron(0,0,input);
        nn.spreading();
        cout << setw(COLUMN_WIDTH) << input << setw(COLUMN_WIDTH) << f(input) << setw(COLUMN_WIDTH) << nn.getNeuron(nn.getSize()-1,0) << setw(COLUMN_WIDTH) << (nn.getNeuron(nn.getSize()-1,0)-f(input)) <<endl;
    }
    while(GetKeyState(VK_ESCAPE) & 32768);
    while(!(GetKeyState(VK_ESCAPE) & 32768));
    return 0;
}

void gotoXY(int x, int y)
{
    static HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
    static COORD CursorPosition;
    CursorPosition.X = x;
    CursorPosition.Y = y;
    SetConsoleCursorPosition(console,CursorPosition);
}
