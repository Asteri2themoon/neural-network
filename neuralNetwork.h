#ifndef RESEAUNEURONE_H_INCLUDED
#define RESEAUNEURONE_H_INCLUDED

#define activate(x) (1/(1+exp(-x)))
#define deriv_activate(x) (x*(1-x))
/*#define activate(x) (tanh(x))
#define deriv_activate(x) (1-(x*x))*/

class neuralNetwork
{
public:
    neuralNetwork();
    neuralNetwork(unsigned int s,unsigned int *nl);
    void connect();
    void setSize(unsigned int size);
    void setSize(unsigned int s,unsigned int *nl);
    unsigned int getSize();
    void setLayer(unsigned int l,unsigned int nS);
    unsigned int getLayerSize(unsigned int l);
    void setNeuron(unsigned int l,unsigned int n,float value);
    float getNeuron(unsigned int l,unsigned int n);
	void setConnection(unsigned int l,unsigned int n1,unsigned int n2,float ratio);
	float getConnection(unsigned int l,unsigned int n1,unsigned int n2);
	void setCorrection(float c);
	float getCorrection();
    bool load(char* dir);
    bool save(char* dir);
    void spreading();
    void reset();
    void learn(float y);
    ~neuralNetwork();
protected:
    unsigned int n_size;
    unsigned int *n_layer;
    unsigned int *n_neuronStateLayer;
    float *n_neuronState;
    unsigned int *n_matrixLayer;
    float *n_matrix;
    float n_correction;
    bool n_connected;
};

#endif // RESEAUNEURONE_H_INCLUDED
